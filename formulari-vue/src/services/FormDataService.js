import http from "../http-common";

class FormDataService {
    getFormData() {
        return http.get("/formData")
    }

    createFormEntry(data) {
        return http.post("/userData", data);
    }
}

export default new FormDataService();